const fs = require('fs')

process.on('SIGUSR2', _ => {
  try {
    const script = fs.readFileSync('./update').toString()
    const func = new Function(script)
    const init_date = (new Date).toJSON()
    const hashs = '#'.repeat(100)

    fs.appendFileSync('./output', `
${hashs}
New script injection
Init date: ${init_date}
${hashs}
${script}
${hashs}
The script is running...`)

    const ret = func()
    const end_date = (new Date).toJSON()

    fs.appendFileSync('./output', `
End date: ${end_date}
The return of the function is
${ret}`)

  } catch(error) {
    console.log('SIGUSR2 error', error)
  }
})

global.i = 0
global.interval = setInterval(_ => {
  i = i + 1
  console.log(i)
}, 1e3)

console.log('PID', process.pid)

